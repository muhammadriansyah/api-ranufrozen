package order

import (
	"fmt"
	"time"
)

type Order struct {
	Id   int
	Date time.Time
	Sum  float64
}

func (order Order) display() {
	fmt.Println("sum: ", order.Sum)
}
